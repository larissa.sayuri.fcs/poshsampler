ui <- fluidPage(
  
  fluidRow(
    column(3, img(width = 200, src = "WEARELADIEShoriz.png")),
    column(6, HTML('<center><p style = "font-family:Ubuntu,Ubuntu,serif;font-style:bold;font-size:40px"> </p></center>')), 
    column(3, HTML('<left><h4><a> https://rladiesbh.com.br/ </a></h4></left>'))
  ),
  
  HTML('<hr style="color: white;">'),
  
  fluidRow(
    column(width = 6,
           wellPanel(
             h2("Enter your list", style = "color:#fff; font-family:Ubuntu"),
             textAreaInput(inputId = "list", 
                           label = NULL,  
                           height = "80vh", 
                           value = lista),
             style = "background-color:#824b9b"
           )
           
    ),
    column(width = 6, 
           wellPanel(
             fluidRow(
               h2("Settings", style = "color:#fff; font-family:Ubuntu"),
               HTML("<center>"),
               column(width = 12, 
                      
                      h3("Sample size", style = "color:#fff; font-family:Ubuntu"),
                      # Input: Select number of rows to display ----
                      numericInput(inputId = "sample_size", 
                                   label = NULL, 
                                   value = 3,
                                   min = 1,
                                   max = 1000000, 
                                   width = "33%")
                      
               ),
               column(width = 4, offset = 4, 
                      fluidRow(
                        actionButton(inputId = "run", 
                                     label = "Run!", 
                                     icon("play-circle-o")),
                        actionButton(inputId = "restart", 
                                     label = "Restart!", 
                                     icon("play-circle-o"))
                      )
               ),

                 column(width = 12,
                        
                        fluidRow(
                          h3("Lucky ones:", style = "color:#fff; font-family:Ubuntu"),
                          tableOutput("sample"),
                          style = "color:#fff"
                        )
   
                 ),
                 HTML("</center>"),
                 style = "margin-right: 0; margin-left: 0;"
               

             ),
             style = "background-color:#824b9b"
           )
    )
  ) # fluidRow
  
)